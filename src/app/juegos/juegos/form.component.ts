import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Company } from 'src/app/companies/company';
import { Juego } from '../juego';
import {JuegoService} from '../juego.service';
import {CompanyService} from '../../companies/company.service';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '../../alert/alert.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  juego: Juego = new Juego();
  companies: Company[];
  title: string = 'Crear Juego';
  categorias: any[] = 		[{title:'Shooter',
                            value:'SHOOTER'}, 
                            {title:'Moba',
                            value:'MOBA'}, 
                            {title:'RPG',
                            value:'RPG'}, 
                            {title:'MMORPG',
                            value:'MMORPG'}, 
                            {title:'Roguelike',
                            value:'ROGUELIKE'}, 
                            {title:'Metroidvania',
                            value:'METROIDVANIA'}];
  constructor(private companyService: CompanyService,
    private juegoService: JuegoService, 
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService) { }

  ngOnInit(): void {
    this.loadCompanies();
    this.loadJuego();
  }

  create(): void {
    //console.log(this.juego);
    this.juegoService.create(this.juego).subscribe(juego=>{
      this.alertService.success(`Se ha creado correctamente el juego: "${juego.titulo}" con ID:"${this.juego.idJuego}"`,
      {autoClose:true});
      this.router.navigate(['/juegos'])
    }
    );
  }

  public update(): void {
    this.juegoService.updateJuego(this.juego).subscribe(juego=>{
      this.alertService.success(`Se ha actualizado correctamente el juego: "${juego.titulo}" con ID:"${this.juego.idJuego}"`,
      {autoClose:true});
      this.router.navigate(['/juegos'])
    }
    );
  }

  loadCompanies(): void{
    this.companyService.getCompanies().subscribe(
      companies =>this.companies = companies
    );
  }

  loadJuego(): void{
    this.activatedRoute.params.subscribe(
      params=> {
        const id = params['id']
        if (id){
          this.title ='Editar Juego';
          this.juegoService.getJuego(id).subscribe(
            juego =>this.juego = juego
          )
        } else{
          this.title = 'Crear Juego';
        }

      })
  }

  compareCompany(companyToCompare: Company, companySelected: Company){
    if(!companyToCompare || !companySelected){
      return false;
    }

    return companyToCompare.idCompany == companySelected.idCompany;
  }

}
