import { Injectable } from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import { Company } from './company';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';

@Injectable()
export class CompanyService {

  urlServer: String = 'http://localhost:8090/';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient, private alertService: AlertService, private router: Router, private loginService: LoginService) { }

  getCompanies(): Observable<Company[]> {
    //return of(JUEGOS);
    //return this.http.get<Juego[]>(this.urlServer + 'companies');
    return this.http.get<Company[]>(this.urlServer + 'companies',{headers:this.loginService.getAuthHeaders()}).pipe(
      catchError(e => {
        console.error(`getCompanies error: "${e.message}"`);

        if(e.status==401){
          this.router.navigate(['/login']);
        }else {
          this.alertService.error(`Error al consultar las compañías: "${e.message}"`);
        }

        return throwError(e);
      })
    );
  }

}
