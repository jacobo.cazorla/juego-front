import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { JuegosComponent } from './juegos/juegos.component';
import { CompaniesComponent } from './companies/companies.component';
import { JuegoService } from './juegos/juego.service';

import { CompanyService } from './companies/company.service';
import { AlertComponent } from './alert/alert.component';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent as JuegosFormComponent } from './juegos/juegos/form.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/juegos', pathMatch: 'full'},
  {path: 'juegos', component: JuegosComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: 'juegos/form/:id', component: JuegosFormComponent},
  {path: 'juegos/form', component: JuegosFormComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    JuegosComponent,
    CompaniesComponent,
    AlertComponent,
    JuegosFormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [JuegoService,
              CompanyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
